package cz.vutbr;

public class SinglzLinkedList {
    private Node head;
    private Node tail;

    public void addNodeToEnd(int data) {
        Node newNode = new Node(data);

        if(head == null) {
            head = newNode;
            tail = newNode;
        }
        else{
            tail.setNext(newNode);
            tail = newNode;
        }
    }

    public void addNodeToStart(int data){
        Node newNode = new Node(data);

        if(head == null){
            head = newNode;
            tail = newNode;
        }
        else {
            Node current = head;
            head = newNode;
            head.setNext(current);
        }
    }

    public void deleteFirstNode() {
        Node current = head;

        if(head == null){
            System.out.println("It is not possible to delete node which doesn't exist.");
        }else{
            head = current.getNext();
        }
    }

    public void deleteLastNode() {
        if(head == null){
            System.out.println("It is not possible to delete node which doesn't exist.");
        }else{
            if(head != tail) {
                Node current = head;
                while(current.getNext() != tail) {
                    current = current.getNext();
                }
                tail = current;
                tail.setNext(null);
            }
            else {
                head = tail = null;
            }
        }
    }

    public void display(){
        Node current = head;

        if(head==null){
            System.out.println("The list is empty.");
        }
        while(current != null){
            System.out.println(current.getData() + " ");
            current = current.getNext();
        }
        System.out.println();
    }

}
